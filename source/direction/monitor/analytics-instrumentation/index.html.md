---
layout: markdown_page
title: Product Direction - Analytics Instrumentation
description: "Analytics Instrumentation manages a variety of technologies that are important for GitLab's understanding of how our users use our products. Learn more here!"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

As part of our overall [3 year Strategy](https://about.gitlab.com/company/strategy/#three-year-strategy), GitLab is focused on [customer results](https://about.gitlab.com/company/strategy/#1-customer-results). In order to empower our customers to achieve their goals and to enable GitLab to ship a world class DevOps product, we must provide the necessary instrumentation interfaces so teams can identify opportunities, mitigate risks, and make the right decisions. This helps our users and us achieve our goals by enabling us to make data-driven decisions.

To accomplish this, we aim to leverage our deep expertise, tied with investments in a strong technical foundation, to enable our users and all teams within GitLab to instrument their applications so that they can then analyze and report on the data those applications create. Like many top SaaS software companies, we are moving to be a product-led and data-driven organization. Through a partnership with our Data Team and collaboration with Product and Engineering we are cultivating a strong data focused culture within GitLab and for our users.

In addition to using data to drive decisions within GitLab, ensuring customer results means we will provide the platform and tools for them to gather data about their own apps. This means they can also make data-driven decisions to improve their products and achieve their business goals.

## Guiding Principles

The key thesis of our group is that providing more visibility into how GitLab is used allows us to make better decisions which lead to better business outcomes for ourselves and our users. In order to build the best DevOps product we can and provide the most value for our customers, we need to collect and analyze usage data across the entire platform to investigate trends, patterns, and opportunities. Insights generated from our Analytics Instrumentation program enable GitLab to identify the best place to invest time and resources, which categories to push to maturity faster, where our UI experience can be improved, and how product changes effect the business.

We understand that usage tracking is a sensitive subject, and we respect and acknowledge our customers' concerns around what we track, how we track it, and what we use it for. We will always be transparent about what we track and how we track it. In line with our company's value of [transparency](https://about.gitlab.com/handbook/values/#transparency), and our [commitment to individual user privacy](https://about.gitlab.com/handbook/product/analytics-instrumentation-guide/service-usage-data-commitment/), our tracking source code and documentation will always be public.

As we build solutions for GitLab and our users to instrument their apps, aspects we will focus on are:

* Usability: We will ensure that it is straightforward to add instrumentation to relevant parts of the app across a wide range of use cases. If it is hard to add instrumentation, users will give up and not do it.
* Scalability: We will ensure the systems we build and provide are robust and suitable for production-grade loads. If users cannot trust the systems we provide to be usable and reliable, they will look for alternatives.
* Manageability: We will ensure that it is possible to easily understand what parts of the app are instrumented and what sort of data will be produced. If it is not clear what is already instrumented, people may not be aware of what their current data options are, re-implement the same event multiple times, or remove an event that someone else is using.
* Adaptability: GitLab's code base is ever evolving. Instrumentation code needs to adapt to changes in where or how a feature is triggered, to keep resulting events consistent as well as not get into the way of feature changes.

<iframe width="560" height="315" src="https://www.youtube.com/embed/_D-C8B5vPS0" frameborder="0" allowfullscreen>
</iframe>

*In this [GitLab Unfiltered video](https://youtu.be/_D-C8B5vPS0) Niko is talking about the groups's approach to Analytics Instrumentation tools improvements and evolution*

### Our opinion on automated tracking

It is a valid question to ask why analytics instrumentation can not be automated to a high extent by automatically tracking API hits based on the URL or button clicks in the UI.
While this would be the most usable solution, as it does not require any manual instrumentation, it is neither scalable nor manageable or adaptable:

* It would multiply the amount of events we receive, which would affect the effort needed for event processing. Querying the useful events will become slower and/or more costly due to the high amount of unneeded events. This can impact the reliability of the system as well.
* It increases the likelihood of personal data unintentionally leaking into event collection as API request parameters or button texts are collected.
* It is not clear which events are useful and which are not since there is no intent behind tracking a specific event. The same user interaction could be tracked through multiple automated events, like a UI click or an API hit.
* It does not obviate the need for instrumentation, since not all events can be automatically tracked, such as the results of an asynchronous computation.
* It creates a dependency between feature code and analytics code since automatic tracking by definition depends on implementation details such as the URI of an API call or the CSS selector of a button. This can lead to a resistance to feature changes since they could break tracking. Analytics instrumentation aims to benefit future feature development not hinder it.

Instrumentation should be easy as possible while still clearly documenting the intent to track a specific behavior and getting out of the way of feature changes.

## Personas that we work with

A key aspect of aligning on our direction is understanding who we are building for. This allows us to best understand the problems they may have and the context that they will be approaching our offerings with.

### Product groups within GitLab
Product [groups](https://about.gitlab.com/handbook/product/categories/) within GitLab consist of a Product Manager, Engineering Manager, engineers, and other stable counterparts. These groups implement new features in GitLab and want to understand how users interact with those features.
These teams have understanding of how the GitLab code base works as it relates to their features, but not necessarily how the instrumentation APIs and architecture work. They are not necessarily aware of the end-to-end story about how information flows from when a user clicks a button to a result being shown in Sisense or a report.

These product groups are our primary customer that we are serving and developing solutions for.

#### Product Manager personas

Consider reading more about [Parker](https://about.gitlab.com/handbook/product/personas/#parker-product-manager), our Product Manager persona.

Product Managers within these groups will have an understanding of how their group's features work from a user perspective, the problem those features solve, and what sorts of actions result in a user "succeeding" at the [job to be done](https://about.gitlab.com/handbook/product/ux/jobs-to-be-done/), and have growth and usage goals for that feature. They will be able to describe a user journey and key points that should be instrumented along that journey to measure success or need for improvement. They will not necessarily understand what the underlying code for the feature looks like or how all the technology pieces fit together. They need to be able to easily understand which kinds of tracking are available and how they are differentiated to be able to understand the resulting data.
If they find it difficult to add the instrumentation they want, they will instead rely solely on qualitiative analysis, such as direct user conversations, rather than a blend of both qualititative and quantitative analysis. 

#### Engineer personas

Consider reading more about [Sasha](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer), our Software Developer persona.

Engineers within these groups will have an understanding of how GitLab is built and run but likely are not familiar with the product instrumentation architecture nor APIs. They heavily rely on documentation, examples, and previous MRs to add instrumentation that their PM requests. When they are unable to self-serve, they will ask the Analytics Instrumentation group for help or give up.

This persona relies on what we provide to them, which means it is critical for us to keep examples up to date and have clear guidance around deprecated APIs so that engineers use our newer, preferred APIs instead of older ones.

### GitLab Data program

The GitLab [data program](https://about.gitlab.com/handbook/business-technology/data-team/#data-program-teams) is responsible for surfacing data and data-driven insights to the business. They have expertise in building data pipelines, models, and managing data once collected.
They are not necessarily familiar with the GitLab code base and rely on product groups to add instrumentation for new metrics or update existing ones.
They rely on Analytics Instrumentation to effectively collect and send data to Snowflake, which is their main interface with the data.

### Customer success

Customer Success team members play a crucial role in partnering with clients to ensure they realize the full value of GitLab's offerings. Although they are experts in customer engagement and optimizing the GitLab experience, they may not have deep knowledge of the product’s instrumentation details. They rely on the Analytics Instrumentation group to help them easily find the relevant metrics and data for features and capabilities and to provide guidance and support on implementing missing instrumentation where needed. Effective support hinges on our ability to assist them in accessing the metrics they need and offering clear, actionable guidance for incorporating necessary instrumentation.

### External GitLab users

External GitLab users are a broad category of individuals with different needs and who have different skill sets. These users may be interested in reading about what data we collect and how to interact with it. In the future, external users will use the application instrumentation SDKs our group provides to be able to instrument their own apps.

External users rely on our handbook pages and sites like [metrics.gitlab.com](https://metrics.gitlab.com/) to understand what data is collected from their GitLab use, how to view it, and how to interact with it. If they are unable to get clear answers to their questions, they become frustrated. In that case, they may reach out to their account manager to help them, post on a forum, or stop using GitLab.

External users will use the application instrumentation SDKs we provide to instrument their apps. These teams will be similar to our own product groups within GitLab. That is, PMs will understand user journeys about their features, developers will understand how their own app is built, but neither will be familiar with our instrumentation SDKs. They will rely heavily on our documentation and examples or else they will give up and do something else. 

## Challenges we face in Analytics Instrumentation

- GitLab's [single application approach to DevOps](/handbook/product/single-application/) creates a product that is both wide and deep, encompassing a large collection of features used by many teams within an organization, which are composed of different types of users.
- That depth/breadth makes it exceedingly complex to properly map out and understand how our diverse customer set is using the product and gaining value.
- We currently are unable to provide GitLab the required data to identify opportunities and make the right decisions against them.
- GitLab's MVC approach to product development introduces frequent changes to the product stages and what data is available, making historical trend analysis difficult.
- There are more and more supplementary applications outside of the GitLab instance, such as the [AI gateway](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist), that need their own instrumentation setup to get a full picture of product usage.


## Roadmap (As of 2024-09-20)

| Initiative | Status | Timeframe |
| ---------- | ------ | ---------- |
|  **Internal events**: With internal events, you can now instrument your features easily, without needing to navigate the complexities of underlying systems or choose a system tailored to each specific use case, allowing you to save both time and effort. [More here](https://docs.gitlab.com/ee/development/internal_analytics/internal_event_instrumentation/)          |   `Complete`     | [FY24 Q2](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1941?iid_path=true) |
| **Service ping REST API access**: Self-managed customers on version 16.9 or above can now access product usage data through the Service Ping REST API, making it easier to retrieve and integrate the data with other sources, without the need for manual downloads. [More here](https://docs.gitlab.com/ee/api/usage_data.html) | `Complete` | [FY24 Q4](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4594) |
| **Internal events generator**: With the CLI generator, you no longer have to manually create definition files for your events and metrics—it generates them for you, complete with code examples for instrumentation and testing, saving you time and effort. [More here](https://docs.gitlab.com/ee/development/internal_analytics/internal_event_instrumentation/quick_start.html#defining-event-and-metrics)           | `Complete`        |   [FY24 Q4](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4591)      |
| **Revamped Metrics Dictionary**: We've made it simpler for teams to discover events and metrics, with improved search capabilities, filtering options, visualizations, and links to Tableau dashboards for data viewing. It also includes sample Snowflake queries, the ability to map your events and metrics, and so much more, all while ensuring it remains the single source of truth for all events and metrics instrumented at GitLab. [More here](https://youtu.be/NLquSWfWMII) | `Complete` | [FY25 Q3](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/8373) |
| **Ability to track more than 3 additional property with internal events:** Tracking additional properties with events reduces the need for separate event creation, simplifying the instrumentation process. Capturing these additional properties provides deeper insights into user behavior, enabling teams to derive actionable insights more easily. Additionally, this approach prepares the system for future analysis needs without requiring extensive reconfiguration. [More here](https://docs.gitlab.com/ee/development/internal_analytics/internal_event_instrumentation/quick_start.html#additional-properties) | `Complete` | [FY25 Q2](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/7208) |
| **Automated error tracking:** We implemented Monte Carlo alerts to proactively detect missing data upstream, allowing us to identify and resolve data quality issues earlier in the process. This ensures greater data integrity and enhances the reliability of our systems. [More here](https://gitlab.com/groups/gitlab-org/-/epics/11223) | `Complete`|  |
| **Instrumentation coverage:** In the past, we [collaborated with teams](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/7201) through pairing sessions and close engagement with individual groups to implement instrumentation. These initiatives effectively showcased how straightforward the process can be and resulted in [increased instrumentation rates](https://10az.online.tableau.com/#/site/gitlab/views/AnalyticsInstrumentationCoverage/MetricCoverageTrends?:iid=2). In the current quarter, we are focusing on closing gaps in [Lighthouse metrics instrumentation](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/8378). However, we still need to ensure that all categories have a basic level of instrumentation. We will continue our efforts to enhance instrumentation coverage in the upcoming quarters. | `Ongoing` | [FY24 Q2](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/6893), [FY24 Q3](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/8279) |
| **Improved Service Ping experience:** We'll work on enhancing the Service Ping experience for our customers by reducing processing time and making it easier for both customers and customer service managers to resolve Service Ping errors efficiently. [More here](https://gitlab.com/groups/gitlab-org/-/epics/14059) | `Coming soon` | TBD |
| **Product usage data parity:** We'll focus on ensuring teams can collect the same level of granular product usage data across different deployment types for greater consistency, while also giving customers easy control over the data they choose to share with us. [More here](https://gitlab.com/groups/gitlab-org/architecture/gitlab-data-analytics/-/epics/38) | `Coming soon` | TBD |
| **Standardizing feature instrumentation at GitLab:** We will establish internal events as the sole method for instrumenting features at GitLab, regardless of use case, to maintain consistency and ensure ease of data management, quality, and legal compliance. [More here](https://gitlab.com/groups/gitlab-org/architecture/gitlab-data-analytics/-/epics/13) |`Future` | TBD |
| **GitLab product usage data access for .com customers**: Enabling our customers to access the same product usage data we collect, promoting greater transparency and ensures we provide our .com customers the same benefits we offer our self-managed customers. [More here](https://gitlab.com/groups/gitlab-org/-/epics/12645) | `Future` | TBD |
| **Streamlining the path to actionable data:** Our goal is to help teams move from concept to actionable data more quickly and seamlessly. This means breaking down the instrumentation process into smaller, manageable steps, such as - Enabling PMs to define clear objectives and success metrics upfront, smoothing the handoff between PMs and EMs, embedding instrumentation into the feature development cycle, automatic metric definitions, accelerating the review process and continuously iterating on the effectiveness of instrumentation. Each of these steps will be measured for performance, with clear targets set, and tailored solutions aimed at minimizing roadblocks and accelerating the instrumentation process | `Future` | TBD |
| **Supporting the needs of the business:** Different parts of the business have different needs related to data. Examples could include needing new sources of data added to Service Ping, updating existing metrics, fixing bugs in existing systems, or other projects.  We expect these types of requests to continue, so we will support requests from different stakeholders as they arise | `Ongoing` | N/A |


## How We Work

For more information on Analytics Instrumentation, you can checkout our [Analytics Instrumentation Guide](https://about.gitlab.com/handbook/product/analytics-instrumentation-guide/) which details a [high-level overview of how we make data usable](https://about.gitlab.com/handbook/product/analytics-instrumentation-guide/#analytics-instrumentation-overview), the [Collection Frameworks](https://about.gitlab.com/handbook/product/analytics-instrumentation-guide/#collection-framework) we leverage, our [Metrics Dictionary](https://about.gitlab.com/handbook/product/analytics-instrumentation-guide/#metrics-dictionary), and much more!


## Working Groups and Cross-Functional Initiatives

Analytics Instrumentation provides the necessary frameworks, tooling, and expertise to help us build a better GitLab. Naturally we sit in the middle of many projects, initiatives and OKRs at GitLab. In order to provide clarity and realistic expectations to our stakeholders and customers we practice relentless prioritization ([per Product Principle #6](https://about.gitlab.com/handbook/product/product-principles/)), identifying what is above the line, what is below, and what is unfunded and not possible for us to action on in a given timeline.

This table lists recurring activities that are part of [working groups and cross-functional initiatives](https://about.gitlab.com/company/team/structure/working-groups/).

| Activity                                                                                                                           | Cadence       | Type | Teams Involved                                                              |
|------------------------------------------------------------------------------------------------------------------------------------|---------------|------|-----------------------------------------------------------------------------|
| [GTM Product Usage Data Working Group](https://docs.google.com/document/d/1riUXq1GdavnSWJklrebBeZnzcAl6XATyLod9tR6-AlQ/edit)       | Weekly        | Sync | Fulfillment PMs, Analytics Instrumentation, Data, Customer Success, Sales        |
| [Data & Analytics Program for R&D Teams](https://docs.google.com/document/d/1CRIGdNATvRAuBsYnhpEfOJ6C64B7j8hPAI0g5C8EdlU/edit)     | Every 2 Weeks | Sync | Fulfillment PMs, Analytics Instrumentation, Growth, Data                         |
| [Product ARR Drivers Sync](https://docs.google.com/document/d/1TxcJqOPWo4pP1S48OSMBnb4rysky8dRrRWJFflQkmlM/edit)                   | Monthly       | Sync | Customer Success, Sales, Product Leadership   
| [ClickHouse Datastore](https://about.gitlab.com/company/team/structure/working-groups/clickhouse-datastore/) | Weekly | Sync | Multiple |

## Quick Links

| Resource                                                                                                                          | Description                                               |
|-----------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| [Internal Analytics Docs](https://docs.gitlab.com/ee/development/internal_analytics/)                                              | An implementation guide for Usage Ping                    |
| [Metrics Dictionary](/handbook/product/analytics-instrumentation-guide#metrics-dictionary)                                        | A SSoT for all collected metrics from Usage Ping               |
| [Privacy Policy](/privacy/)                                                                                                       | Our privacy policy outlining what data we collect and how we handle it     |
| [Implementing Product Performance Indicators](/handbook/product/analytics-instrumentation-guide#implementing-product-performance-indicators)                                   | The workflow for putting product performance indicators in place   |
| [Analytics Instrumentation Development Process](/handbook/engineering/development/analytics/analytics-instrumentation/) | The development process for the Analytics Instrumentation groups         |
| [Project resposibilities](/handbook/engineering/development/analytics/analytics-instrumentation/#responsibilities) | List of several projects that our group is the DRI for |
| [Incident Reporting](/handbook/engineering/development/analytics/analytics-instrumentation/#incidents) | Analytics instrumentation specific incident reporting process  |
