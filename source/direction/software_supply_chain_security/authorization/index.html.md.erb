---
layout: markdown_page
title: "Group Direction - Authorization"
description: "Authorization keeps resources secure but accessible."
---

- TOC
{:toc}

| Group | **Authorization** |
| --- | --- |
| Stage | [Govern](https://about.gitlab.com/handbook/product/categories/#govern-stage) |
| Group | [Authorization](https://about.gitlab.com/handbook/product/categories/#authorization-group) |
| Content Last Reviewed | `2024-12-17` |


## Overview

This is the direction page for the Authorization group in the Software Supply Chain Security stage. The Authorization group is responsible for ensuring that an authenticated user has access to the proper resources within the application. Additionally, the group builds capabilities to detect and prevent malicious activity from occurring within GitLab environments.

1. [Permissions](permissions/)
2. [Instance Resiliency](instance_resiliency/)

## Top themes

| Priority | Theme | Target Release | 
| ------ | ------ | ------ |
| 1 | [Add support for granular token permissions to Job Tokens](https://gitlab.com/groups/gitlab-org/-/epics/14804) to allow for fine-grained access in CI/CD workflows. |  18.0     | 
| 2 | [Build Admin Custom Role to support granular permissions for the Admin Area](https://gitlab.com/groups/gitlab-org/-/epics/15069) to allow organizations to reduce the number of admins on self-managed environments. | 17.10 | 
| 3 | [Improve visibility by providing a breakdown of roles and assigned users](https://gitlab.com/groups/gitlab-org/-/epics/13434) to allow for organizations to identify and reduce overprivileged users. |  Deprioritized    | 

## Priorities

| Priority | Name | DRI | Target Release | 
| ------ | ------ | ------ | ------ |
<% data.product_priorities.authorization.priorities.each.with_index do |priority, index| %>
| <%= index + 1 %> | [<%= priority.name %>](<%= priority.url %>) | `<%= priority.dri.presence || "TBD" %>` | `<%= priority.target_release.presence || "TBD" %>`  |
<% end %>

## Jobs to be Done

The UX department has performed a JTBD Canvas for Authorization that can be found on this [epic](https://gitlab.com/groups/gitlab-org/-/epics/13901) and [figma file](https://www.figma.com/board/YmDkQqNEbwHt89mWpiY0lG/).

The main jobs for users related to Authorization capabilities include:

| Main Job                 | Outcomes                                                     |
| ------------------------ | ------------------------------------------------------------ | 
| Provision access rights  | Minimize productivity loss coming from user’s lack of access to resources they need to do their job. <br>Minimize security risk and data breaches coming from bad actors. <br> Reduce manual work when managing user’s role and access to resources. | 
| Maintain access policies | Minimize security risk and data breaches coming from bad actors. <br>Increase compliance in industry related audits (eg SOC II). <br>Standardize organization’s user and resource permission management across all software and applications. | 
| Gain access rights       | Decrease time spent on gaining access rights. <br>Increase productivity <br>Increase team collaboration. |