---
layout: markdown_page
title: "Group Direction - Knowledge"
description: "Knowledge group direction page focusing on wiki, pages, markdown, and text editors"
canonical_path: "/direction/plan/knowledge/"
---

- TOC
{:toc}

## Knowledge group

| | |
| --- | --- |
| Stage | [Plan](/direction/plan/) |
| Content Last Reviewed | `2024-12-04` |

### Introduction

Welcome to the Knowledge group direction page! Knowledge group is in the Plan Stage of GitLab and contains the Wiki, Pages, Markdown, and Text Editors categories. Knowledge Group also manages the development of [GitLab Query Language](https://gitlab.com/groups/gitlab-org/gitlab-query-language/-/wikis/home) (GLQL). These categories, and GLQL, fall within the [knowledge management](https://www.gartner.com/reviews/market/knowledge-management-software) (KM) market as defined by Gartner. The focus of KM is to provide visibility and access to a flow of information across a variety of operations, enabling collaboration that has traditionally existed in silos. Commonly used products in the KM market include GitLab, [Confluence](https://www.atlassian.com/software/confluence), and [Notion](https://www.notion.so/product). 

If you have feedback related to Knowledge Group please comment and/or contribute in these [issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=group%3A%3Aknowledge&first_page_size=20), or create a new issue if you find none fit your criteria. Make sure to tag `@mmacfarlane`, Product Manager for Knowledge group, so he can read and respond to your comment. Sharing your feedback via issues is the one of the best ways to contribute to our strategy and vision! 

### Our vision and current work

At GitLab, our [vision](https://about.gitlab.com/company/vision/) is to become the AllOps platform, a single application for all innovation. Knowledge Group is a signficant contributor to this vision given its focus on enabling collaboration across users regardless of their role. An overview of how each Knowledge Group category aligns with this vision is as follows:

Pages: Our vision for Pages is to provide an experience that guides and supports users to host static assets on the web, regardless of their level of development experience. At the moment we are working on one major customer request, [GitLab Pages Parallel Deployments](https://gitlab.com/groups/gitlab-org/-/epics/10914), which will further enable us to provide the aforementioned experience. We are already evaluating future opportunities around improving the experience of creating and configuring Pages sites and making content management and collaboration easier for non-developer personas. Two ideas include [User Authentication and Authorization](https://gitlab.com/gitlab-org/gitlab/-/issues/426609) and [Offering Audit Logs](https://gitlab.com/gitlab-org/gitlab/-/issues/426604).

Text Editors and Markdown: We are primarily focused on creating location parity between the Rich Text Editor and Plain Text Editor via [introducing the Rich Text Editor across more areas of GitLab](https://gitlab.com/groups/gitlab-org/-/epics/10378). We're confident creating location parity will improve accessibility and allow users who traditionally feel GitLab is too technical a better opportunity to contribute. We are also continuously focusing on advanced editing and performance stabilization.

Wiki: The Wiki is an important part of the Agile Planning workflow. The Wiki was in [stable maintenance mode](https://docs.gitlab.com/ee/administration/maintenance_mode/) for much of FY23 and early FY24. We've recently enhanced the Wiki to include a [print PDF feature](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/129427) and introduced the [rich text editor](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/126855#ec5328d883a8d56f0e029fa5d7c5d2bfb9a520f9) within it. In 16.10 we also introduced templates in the Wiki. We hope to leverage the [JTBD](https://gitlab.com/groups/gitlab-org/-/epics/12826) we've identified for the category to create a solution that effectively compliments our Agile Planning offering.

### 1 year plan

Pages: Our 1 year plan for Pages can be broken into two distinct themes, stability and improvement. 

Our first theme, stability, focuses on the breakdown of existing bugs and maintenance items, as well as updating documentation related to previous releases. Specific to documentation, we've come to understand that some previous feature rollouts did not include updates to technical documentation, which has caused some confusion to users in utilizing the product. 

Our second theme, improvement, focuses on new feature development. We are acutely aware of four major customer requests as detailed on the [Pages Direction Page](https://about.gitlab.com/direction/plan/knowledge/pages/). We aim to prioritize these items to improve our customer experience, and are thankful to our customers for their patience and feedback as we look to deliver these items.

Text Editors and Markdown: Our 1 year plan for Text Editors and Markdown revolves around the theme of GitLab as an AllOps plaftorm. We hope to implement the new Rich Text Editor across GitLab to improve accessibility and efficiency for all users of GitLab. You can follow our progress in the [General Availability Epic](https://gitlab.com/groups/gitlab-org/-/epics/10378).

Wiki: In FY24Q2 we completed research on the Wiki category in order to develop JTBD. We've finalized these [JTBD](https://gitlab.com/groups/gitlab-org/plan-stage/-/epics/54) and will utilize them to inform our Wiki strategy.

### What we recently completed

Text Editors and Markdown: [Rich Text Editor: Release Rich Text Editor Generally](https://about.gitlab.com/releases/2023/07/22/gitlab-16-2-released/#:~:text=The%20rich%20text%20editor%20is%20a%20new%20way%20of%20editing,can%20follow%20our%20progress%20here.), [Display and Edit Markdown Comments in the Rich Text Editor](https://gitlab.com/gitlab-org/gitlab/-/issues/342173), [Insert Table of Contents in the Rich Text Editor](https://gitlab.com/gitlab-org/gitlab/-/issues/366525).

Wiki: [Draw.io/diagrams.net integration with wiki](https://gitlab.com/gitlab-org/gitlab/-/issues/20305/?_gl=1*lfhu8t*_ga*MTU5MDI5ODc5NS4xNjY1NTkyMzcy*_ga_ENFH3X7M5Y*MTY4MTQwNDI2MC40MDYuMS4xNjgxNDA1MzI3LjAuMC4w), [Editing Sidebar in Project or Group Wiki Removes Existing Sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/378028),Relative Links are [Broken On Wiki ASCII Pages](https://gitlab.com/gitlab-org/gitlab/-/issues/377976), [templates in wiki](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/133279), and [autocomplete](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/133281).

Pages: [GitLab Pages without DNS Wildcard](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/131947) and [GitLab Pages Parallel Deployments Beta](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/132384).

### What we are currently working on

Pages: [GitLab Pages Parallel Deployments GA](https://gitlab.com/groups/gitlab-org/-/epics/10914)
 
Rich Text Editor: [Enabling the rich text editor across GitLab](https://gitlab.com/groups/gitlab-org/-/epics/10378)

Wiki: [GLQL implementation in GitLab, including Wiki](https://gitlab.com/groups/gitlab-org/-/epics/14767)

### Competitive analysis and best-in-class landscape

Knowledge Group Competitive Statement: The focus of Knowledge group is to provide visibility and access to a flow of information across a variety of operations, enabling collaboration that has traditionally existed in silos. Commonly used products in the market include Confluence, Notion, and GitHub. We believe that fundamental improvements in our Pages, Text Editors, Markdown, and Wiki categories enable us to deliver a more competitive planning experience against industry leaders.

Pages: We are invested in supporting the process of developing and deploying code from a single place as a convenience for our users. Other providers, such as Netlify, deliver a more comprehensive solution. There are project templates available that offer the use of Netlify for static site CI/CD, while also still taking advantage of GitLab for SCM, merge requests, issues, and everything else. GitLab offers [configurable redirects](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/24), a well-loved feature of Netlify, made available in gitlab-pages. We are seeing a rise in JAMStack and static site generators partnering in the media. This trend toward API-first, affirms our modernization effort of Pages, reinforcing our cloud native installation maturity plan. GitHub also offers hosting of static sites with GitHub Pages. Key differentiators between the two are that GitHub Pages configuration and deployment is more "automatic" in that it doesn't require you to edit a CI configuration file, and that GitHub Pages has limits placed on bandwidth, builds, and artifact size where GitLab currently does not.

Text Editors and Markdown: These two categories do not necessarily have a direct competitor but they are important to our vision of becoming an AllOps platform.

Wiki: We currently most closely compete with GitHub Wiki but we would like to compete with Confluence, Notion, Roam Research, and Google Docs. We've heard from customers that managing wikis with tens of thousands of pages can be challenging. And while a full-featured product like Confluence has advanced features and integrations, the GitLab wiki would be a stronger competitor if we improved our sidebar structure and parity between Group and Project Wikis.