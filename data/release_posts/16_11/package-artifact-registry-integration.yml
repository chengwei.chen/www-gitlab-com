---
features:
  secondary:
  - name: "Connect Google Artifact Registry to your GitLab project"
    available_in: [free, silver, gold]
    documentation_link: 'https://docs.gitlab.com/ee/user/project/integrations/google_artifact_management.html'
    reporter: trizzi
    stage: package
    categories:
    - 'Container Registry'
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/12365'
    description: |
      You use the GitLab container registry to view, push, and pull Docker and OCI images alongside your source code and pipelines. For many GitLab customers, this works great for container images during the `test` and `build` phases. But, it's common for organizations to publish their production images to a cloud provider, like Google.
      
      Previously, to push images from GitLab to Google Artifact Registry, you had to create and maintain custom scripts to connect and deploy to Artifact Registry. This was inefficient and error prone. In addition, there was no way easy way to get a holistic view of all of your container images. 

      Now, you can leverage the new Google Artifact Management feature to easily connect your GitLab project to an Artifact Registry repository. Then you can use GitLab CI/CD pipelines to publish images to the Artifact Registry. You can also view images that have published to the Artifact Registry in GitLab by going to **Deploy > Google Artifact Registry**. To view details about an image, simply select an image.
      
      This feature is in Beta and is currently available only on GitLab.com.
