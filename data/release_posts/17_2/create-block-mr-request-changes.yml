features:
  primary:
  - name: "Block a merge request by requesting changes"
    available_in: [premium, ultimate]  # Include all supported tiers
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/reviews/index.html#prevent-merge-when-you-request-changes'
    image_url: '/images/17_2/create-block-mr-request-changes.png'
    reporter: phikai
    stage: create # Prefix this file name with stage-informative-title.yml
    categories:
      - 'Code Review Workflow'
    issue_url: # Multiple links are supported. Avoid linking to confidential issues.
      - 'https://gitlab.com/gitlab-org/gitlab/-/issues/761'
    epic_url:
      - 'https://gitlab.com/groups/gitlab-org/-/epics/11719'
    description: |
      When you perform a review, you can complete it by choosing whether to `approve`, `comment`, or `request changes` ([released in GitLab 16.9](https://about.gitlab.com/releases/2024/02/15/gitlab-16-9-released/#request-changes-on-merge-requests)). While reviewing, you might find changes that should prevent a merge request from merging until they're resolved, and so you complete your review with `request changes`. 

      When requesting changes, GitLab now adds a merge check that prevents merging until the request for changes has been resolved. The request for changes can be resolved when the original user who requested changes re-reviews the merge request and subsequently approves the merge request. If the user who originally requested changes is unable to approve, the request for changes can be **Bypassed** by anyone with merge permissions, so development can continue.

      Leave us feedback about this new feature in [issue 455339](https://gitlab.com/gitlab-org/gitlab/-/issues/455339).
