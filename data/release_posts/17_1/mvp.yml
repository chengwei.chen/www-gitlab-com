---
mvp:
  fullname: ['Shubham Kumar', 'Joe Snyder']
  gitlab: ['imskr', 'joe-snyder']
  description: |
    Everyone can [nominate GitLab's community contributors](https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/issues/490)!
    Show your support for our active candidates or add a new nomination! 🙌

    Shubham Kumar [completed 7 issues during 17.1](https://gitlab.com/dashboard/issues?sort=due_date_desc&state=closed&assignee_username%5B%5D=imskr&milestone_title=17.1)
    and has been consistently contributing to GitLab since 2021.
    He has now reached over 50 merged contributions!
    Shubham is a [GitLab Hero](https://about.gitlab.com/community/heroes/) and a former Google Summer of Code contributor.

    Shubham was nominated by [Christina Lohr](https://gitlab.com/lohrc), Senior Product Manager at GitLab.
    "Shubham has helped with a lot of issues over the past weeks and months, specifically with closing gaps in our API offering," says Christina.
    "I cannot write release posts fast enough for all the additions that Shubham is pushing through!"

    "The open-source community is amazing," says Shubham.
    "I am grateful for the opportunity and recognition, and I look forward to continuing my contributions to the GitLab platform."

    Joe Snyder was nominated by [Kai Armstrong](https://gitlab.com/phikai), Principal Product Manager at GitLab,
    for building a much requested feature for [restricting diffs from being included in emails](https://gitlab.com/gitlab-org/gitlab/-/issues/24733).
    This contribution took more than 10 merge requests going back to GitLab 15.3.
    "This is a massive feature that's taken many milestones, complicated migrations, and changes to the product to enable it's support," says Kai.
    "Joe worked tirelessly with many maintainers and collaborators over the milestones to get this work done."

    [Jocelyn Eillis](https://gitlab.com/jocelynjane), Product Manager at GitLab, supported Joe's nomination
    by highlighting additional work to fix a bug where [nested variables in `build:resource_group` are not expanded](https://gitlab.com/gitlab-org/gitlab/-/issues/361438).
    "This bug had 23 upvotes in addition to documented customer demand in the issue itself," says Jocelyn.
    "The quick turnaround on reviewer feedback means we were able to get this into GitLab 17.1!"

    This is Joe's second GitLab MVP after previously being awarded in [GitLab 16.6](https://about.gitlab.com/releases/2023/11/16/gitlab-16-6-released/#mvp).
    Joe is a Senior R&D Engineer at [Kitware](https://www.kitware.com/) and has been contributing to GitLab since 2021.
