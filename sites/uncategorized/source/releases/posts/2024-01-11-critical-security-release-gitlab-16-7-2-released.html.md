---
title: "GitLab Critical Security Release: 16.7.2, 16.6.4, 16.5.6"
categories: releases
author: Greg Myers
author_gitlab: greg
author_twitter: gitlab
description: "Learn more about GitLab Critical Security Release: 16.7.2, 16.6.4, 16.5.6 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/01/11/critical-security-release-gitlab-16-7-2-released/'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 16.7.2, 16.6.4, 16.5.6 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases fixes for security vulnerabilities in security releases.
For more information, you can visit our [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of our regular and security release blog posts [here](/releases/categories/releases/).
In addition, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest security release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**. If you have not upgraded yet, be aware that there is [a newer patch](https://about.gitlab.com/releases/2024/01/12/gitlab-16-7-3-released/) that includes additional fixes for recently discovered DB migration issue. Please upgrade to 16.7.3, 16.6.5, 16.5.7, or newer to prevent the migration issue.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Table of fixes

| Title | Severity |
| ----- | -------- |
| [Account Takeover via password reset without user interactions](#account-takeover-via-password-reset-without-user-interactions) | Critical |
| [Bypass CODEOWNERS approval removal](#bypass-codeowners-approval-removal) | High |
| [Attacker can abuse Slack/Mattermost integrations to execute slash commands as another user](#attacker-can-abuse-slackmattermost-integrations-to-execute-slash-commands-as-another-user) | High |
| [Workspaces able to be created under different root namespace](#workspaces-able-to-be-created-under-different-root-namespace) | Medium |
| [Commit signature validation ignores headers after signature](#commit-signature-validation-ignores-headers-after-signature) | Low |

### Account Takeover via Password Reset without user interactions

An issue has been discovered in GitLab CE/EE affecting all versions from 16.1 prior to 16.1.6, 16.2 prior to 16.2.9, 16.3 prior to 16.3.7, 16.4 prior to 16.4.5, 16.5 prior to 16.5.6, 16.6 prior to 16.6.4, and 16.7 prior to 16.7.2 in which user account password reset emails could be delivered to an unverified email address.
This is a Critical severity issue (`CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H`, 10.0).
It is now mitigated in the latest release and is assigned [CVE-2023-7028](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-7028).

This security fix has been backported to GitLab versions and 16.1.6, 16.2.9, 16.3.7, and 16.4.5 in addition to 16.5.6, 16.6.4, and 16.7.2.

Thanks [asterion04](https://hackerone.com/asterion04) for reporting this vulnerability through our HackerOne bug bounty program.

### FAQ

**What should I do if I believe my GitLab instance is compromised?** 

_In addition to following your incident response plan_

1. Apply the Critical Security Release to your GitLab instance
2. Enable [Two-Factor Authentication](https://docs.gitlab.com/ee/security/two_factor_authentication.html) (2FA) for all GitLab accounts
3. Rotate all secrets stored in GitLab:
    1. All credentials, including GitLab account passwords
    2. API tokens
    3. Any certificates
    4. Any other secrets
4. Follow steps in our incident response guide, [here](https://docs.gitlab.com/ee/security/responding_to_security_incidents.html#suspected-compromised-user-account)

**Who is impacted by this?** 

GitLab self-managed instances using the following affected versions:

- 16.1 to 16.1.5
- 16.2 to 16.2.8
- 16.3 to 16.3.6
- 16.4 to 16.4.4
- 16.5 to 16.5.5
- 16.6 to 16.6.3
- 16.7 to 16.7.1

Within these versions, all authentication mechanisms are impacted.
Additionally, users who have two-factor authentication enabled are vulnerable to password reset but not account takeover as their second authentication factor is required to login.

**What actions should I take?**

- Upgrade self-managed instances to a [patched version](https://about.gitlab.com/releases/categories/releases/) following our upgrade path. Do not skip upgrade stops as this could create instability.
  - Note: 16.3.x is a [required upgrade stop](https://docs.gitlab.com/ee/update/#required-upgrade-stops) in the [GitLab upgrade path](https://docs.gitlab.com/ee/update/#upgrade-paths).
- Enable [Two-Factor Authentication](https://docs.gitlab.com/ee/security/two_factor_authentication.html) (2FA) for all GitLab accounts, especially for users with elevated privileges (e.g. administrator accounts).

**Has the vulnerability been resolved?**

This vulnerability was resolved with this security release.

**Were any accounts actually compromised due to this vulnerability?**

We have not detected any abuse of this vulnerability on platforms managed by GitLab, including GitLab.com and GitLab Dedicated instances. Self-managed customers can review their logs to check for possible attempts to exploit this vulnerability: 

- Check [gitlab-rails/production_json.log](https://docs.gitlab.com/ee/administration/logs/#production_jsonlog) for HTTP requests to the `/users/password` path with params.value.email consisting of a JSON array with *multiple* email addresses.
- Check [gitlab-rails/audit_json.log](https://docs.gitlab.com/ee/administration/logs/#audit_jsonlog) for entries with `meta.caller_id` of `PasswordsController#create` and `target_details` consisting of a JSON array with *multiple* email addresses.

**When was the vulnerability introduced?**

The vulnerability was introduced in 16.1.0 on May 1, 2023. 

**How was the vulnerability discovered?**

The vulnerability was responsibly reported through our Bug Bounty program.

**What security measures do you have in place to prevent such vulnerabilities?**

- We have added multiple tests that validate the password reset logic as a whole, in particular handling of email provided, the email generation, and content to prevent similar vulnerabilities.
- Security reviews are a required part of the MR checklist that developers must complete.
- We have a code review process that requires multiple approvals for changes.
- We have started the [Root Cause Analysis process](https://handbook.gitlab.com/handbook/security/root-cause-analysis/) in order to determine a comprehensive list of follow-up actions, including ways to prevent vulnerabilities like this one.
- We have a [two-factor authentication feature](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html) that prevents such a vulnerability if enabled. It is currently enabled for all GitLab Team Members.
- We have added additional developer documentation in the code base to ensure implementation and security considerations are available to engineers working in this area in the future.
- Revised the implementation logic to not support the submission of multiple email addresses for reset links.

**How did this happen?**

A change was made in 16.1.0 to allow users to reset their password through a secondary email address. The vulnerability is a result of a bug in the email verification process. The bug has been fixed with this patch, and as mentioned above, we have implemented a number of preventive security measures to protect customers.

**Does this affect me if I use an Identity Provider, like Okta or Azure AD?**

Users without SSO enforcement are vulnerable. If your configuration allows a username and password to be used in addition to SSO options, then you are impacted. Disabling all password authentication options via [Sign-in restrictions settings](https://docs.gitlab.com/ee/administration/settings/sign_in_restrictions.html#password-authentication-enabled) will mitigate the vulnerability for Self-Managed customers that have an external identity provider configured, as this will disable the ability to perform password reset.

**Am I affected by this vulnerability if I have 2FA enforced?**

An attacker will not be able to takeover your account if you have 2FA enabled. They may still be able to reset your password but will not be able to access your second factor authentication method. If you are suddenly redirected to login, or see a reset email triggered, please reset your password.

**Does this vulnerability affect GitLab Runner?**

No, this vulnerability does not affect GitLab Runner. This vulnerability affected the GitLab Rails codebase for impacted versions of GitLab itself. GitLab Runner has a separate code base that is unaffected.

### Bypass CODEOWNERS approval removal

An issue has been discovered in GitLab affecting all versions starting from 15.3 before 16.5.5, all versions starting from 16.6 before 16.6.4, all versions starting from 16.7 before 16.7.2. The required CODEOWNERS approval could be bypassed by adding changes to a previously approved merge request.
This is a high severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:H/A:N`, 7.6).
It is now mitigated in the latest release and is assigned [CVE-2023-4812](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-4812).

Thanks [ali_shehab](https://hackerone.com/ali_shehab) for reporting this vulnerability through our HackerOne bug bounty program.

### Attacker can abuse Slack/Mattermost integrations to execute slash commands as another user

Incorrect authorization checks in GitLab CE/EE from all versions starting from 8.13 before 16.5.6, all versions starting from 16.6 before 16.6.4, all versions starting from 16.7 before 16.7.2, allows a user to abuse Slack/Mattermost integrations to execute slash commands as another user.
This is a high severity issue (`CVSS:3.1/AV:N/AC:H/PR:H/UI:R/S:C/C:H/I:H/A:N`, 7.3).
It is now mitigated in the latest release and is assigned [CVE-2023-5356](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5356).

Thanks [yvvdwf](https://hackerone.com/yvvdwf) for reporting this vulnerability through our HackerOne bug bounty program.

### Workspaces able to be created under different root namespace

An improper access control vulnerability exists in GitLab Workspaces affecting all versions prior to 16.5.6, 16.6 prior to 16.6.4 and 16.7 prior to 16.7.2. This condition allows an attacker to create a workspace in one group that is associated with an agent from another group.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:H/PR:H/UI:N/S:C/C:H/I:L/A:N`, 6.6).
It is now mitigated in the latest release and is assigned [CVE-2023-6955](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6955).

This vulnerability was discovered internally by GitLab team member [@j.seto](https://gitlab.com/j.seto).

### Commit signature validation ignores headers after signature

An issue has been discovered in GitLab CE/EE affecting all versions from 12.2 prior to 16.5.6, 16.6 prior to 16.6.4, and 16.7 prior to 16.7.2 in which an attacker could potentially modify the metadata of signed commits.
This is a low severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:U/C:N/I:L/A:N`, 3.5).
It is now mitigated in the latest release and is assigned [CVE-2023-2030](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-2030).

Thanks [lotsofloops](https://hackerone.com/lotsofloops) for reporting this vulnerability through our HackerOne bug bounty program.

## Non Security Patches

### 16.7.2

* [Backport 16.7: Clean mocked tags from assets](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1688)
* [Backport: Solving database cross joins on HookData::ProjectBuilder](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140347)
* [Fix order-dependent Sidekiq config spec failures](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140621)
* [Harden Internal Events CLI specs against flakiness](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140772)
* [Enable Apollo Boards by default](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140892)
* [Backport "Add missing ci_sources_pipelines indexes for self-host" 16.7](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140934)
* [Temporarily pin Faraday related gems](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7338)

### 16.6.4

* [Merge branch 'ci-clean-mocked-tags' into '16-6-stable'](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1675)
* [Backport 16.6 : Clean mocked tags from assets](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1689)
* [Backport-Search::IndexRepairService using Repository index for projects](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139540)
* [Backport !140718 into 16.6 stable branch](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141018)
* [Temporarily pin Faraday related gems](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7337)

### 16.5.6

* [CI: clean mocked tags from assets, don't pollute artifacts (backport to 16.5)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1678)
* [Backport 16.5 : Clean mocked tags from assets](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1690)
* [Backport 16.5: Fix chatty loopWriter logs when log level config is empty](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6583)
* [Bump allure-report and backport clickhouse version fix to 16.5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141027)
* [Temporarily pin Faraday related gems](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7336)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Security Release Notifications

To receive security release blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [security release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).
