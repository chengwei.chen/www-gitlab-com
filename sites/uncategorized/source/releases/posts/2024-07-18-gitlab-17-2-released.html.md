---
release_number: "17.2" # version number - required
title: "GitLab 17.2 released with log streaming, a new pipeline execution security policy, and vulnerability explanations now generally available" # short title (no longer than 62 characters) - required
author: Viktor Nagy # author name and surname - required
author_gitlab: nagyv-gitlab # author's gitlab.com username - required
image_title: '/images/17_2/17-2-cover.png' # cover image - required
description: "GitLab 17.2 released with log streaming, a new pipeline execution policy, vulnerability explanation now generally available and Duo Chat and Code Suggestions integrations in workspaces and much more!" # short description - required
twitter_image: '/images/17_2/17-2-cover.png' # required - copy URL from image title section above
categories: releases # required
layout: release # required
featured: yes
rebrand_cover_img: true

# APPEARANCE
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
# release_number_image: "/images/X_Y/X_Y-release-number-image.svg" # uncomment if you want a svg image to replace the release number that normally overlays the background image

---

<!--
This is the release blog post file. Add here the introduction only.
All remaining content goes into data/release-posts/.

**Use the merge request template "Release-Post", and please set the calendar due
date for each stage (general contributions, review).**

Read through the Release Posts Handbook for more information:
https://about.gitlab.com/handbook/marketing/blog/release-posts/#introduction
-->

Today, we are excited to announce the release of GitLab 17.2 with [vulnerability explanations becoming generally available and integrated with GitLab Duo](#vulnerability-explanation) to help understand SAST vulnerabilities, [log streaming support for Kubernetes](#log-streaming-for-kubernetes-pods-and-containers) to help troubleshoot workloads without leaving GitLab, a new [pipeline execution security policy type](#pipeline-execution-policy-type) to enforce the execution of CI/CD jobs in pipelines, [Duo Chat and Code Suggestions support in GitLab workspaces](#gitlab-duo-chat-and-code-suggestions-available-in-workspaces) for a productivity boost in the remote development environment, and much more!
These are just a few highlights from the 30+ improvements in this release. Read on to check out all of the great updates below.

To the wider GitLab community, thank you for the 160+ contributions you provided to GitLab 17.2!
At GitLab, [everyone can contribute](https://about.gitlab.com/community/contribute/), and we couldn't have done it without you!

To preview what's coming in next month's release, check out our [Upcoming Releases page](/direction/kickoff/), which includes our 17.3 release kickoff video.
