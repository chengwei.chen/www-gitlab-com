---
title: "GitLab Critical Patch Release: 17.1.1, 17.0.3, 16.11.5"
categories: releases
author: Nikhil George
author_gitlab: ngeorge1
author_twitter: gitlab
description: "Learn more about GitLab Critical Patch Release: 17.1.1, 17.0.3, 16.11.5 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.1.1, 17.0.3, 16.11.5 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases fixes for vulnerabilities in dedicated patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [Run pipelines as any user](#run-pipelines-as-any-user) | Critical |
| [Stored XSS injected in imported project's commit notes](#stored-xss-injected-in-imported-projects-commit-notes) | High |
| [CSRF on GraphQL API `IntrospectionQuery`](#csrf-on-graphql-api-introspectionquery) | High |
| [Remove search results from public projects with unauthorized repos](#remove-search-results-from-public-projects-with-unauthorized-repos) | High |
| [Cross window forgery in user application OAuth flow](#cross-window-forgery-in-user-application-oauth-flow) | Medium |
| [Project maintainers can bypass group's merge request approval policy](#project-maintainers-can-bypass-groups-merge-request-approval-policy) | Medium |
| [ReDoS via custom built markdown page](#redos-via-custom-built-markdown-page) | medium |
| [Private job artifacts can be accessed by any user](#private-job-artifacts-can-be-accessed-by-any-user) | Medium |
| [Security fixes for banzai pipeline](#security-fixes-for-banzai-pipeline) | Medium |
| [ReDoS in dependency linker](#redos-in-dependency-linker) | Medium |
| [Denial of service using a crafted OpenAPI file](#denial-of-service-using-a-crafted-openapi-file) | Medium |
| [Merge request title disclosure](#merge-request-title-disclosure) | Medium |
| [Access issues and epics without having an SSO session](#access-issues-and-epics-without-having-an-sso-session) | Medium |
| [Non project member can promote key results to objectives](#non-project-member-can-promote-key-results-to-objectives) | Low |

### Run pipelines as any user

An issue was discovered in GitLab CE/EE affecting all versions starting from 15.8 prior to 16.11.5, starting from 17.0 prior to 17.0.3, and starting from 17.1 prior to 17.1.1, which could allow an attacker to trigger a pipeline as another user under certain circumstances. This is a critical severity issue (CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:N, 9.6). It is now resolved in the latest release and is assigned CVE-2024-5655.

Thanks to [ahacker1](https://hackerone.com/ahacker1) for reporting this vulnerability through our HackerOne bug bounty program.

Breaking changes:
1. This fix changes the MR re-targeting workflow so that a pipeline will not automatically run when a merge request is automatically re-targeted due to its previous target branch being merged. Users will need  to manually start a pipeline to have CI execute for their changes.
1. GraphQL authentication using CI_JOB_TOKEN is disabled by default from 17.0.0, and back ported to 17.0.3, 16.11.5 in the current patch release. If access to the GraphQL API is required, please configure one of the several supported token types for authentication.

At this time, we have not found evidence of abuse of this vulnerability on the platforms managed by GitLab, including GitLab.com and GitLab Dedicated instances.


### Stored XSS injected in imported project's commit notes

An issue was discovered in GitLab CE/EE affecting all versions starting from 16.9 prior to 16.11.5, starting from 17.0 prior to 17.0.3, and starting from 17.1 prior to 17.1.1, where a stored XSS vulnerability could be imported from a project with malicious commit notes.
This is a high severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:N`, 8.7).
It is now mitigated in the latest release and is assigned [CVE-2024-4901](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-4901).

Thanks [yvvdwf](https://hackerone.com/yvvdwf) for reporting this vulnerability through our HackerOne bug bounty program.


### CSRF on GraphQL API `IntrospectionQuery`

An issue has been discovered in GitLab CE/EE affecting all versions from 16.1.0 before 16.11.5, all versions starting from 17.0 before 17.0.3, all versions starting from 17.1.0 before 17.1.1 which allowed for a CSRF attack on GitLab's GraphQL API leading to the execution of arbitrary GraphQL mutations. This is a high severity issue (`CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:N`, 8.1). It is now mitigated in the latest release and is assigned [CVE-2024-4994](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-4994).

Thanks [ahacker1](https://hackerone.com/ahacker1) for reporting this vulnerability through our HackerOne bug bounty program


### Remove search results from public projects with unauthorized repos

Improper authorization in global search in GitLab EE affecting all versions from 16.11 prior to 16.11.5 and 17.0 prior to 17.0.3 and 17.1 prior to 17.1.1 allows an attacker leak content of a private repository in a public project. This is a high severity issue (`CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N`, 7.5). It is now mitigated in the latest release and is assigned [CVE-2024-6323](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-6323).

Thanks to GitLab Team Member, [@joernchen](https://gitlab.com/joernchen) for reporting this issue.


### Cross window forgery in user application OAuth flow

A Cross Window Forgery vulnerability exists within GitLab CE/EE affecting all versions from 16.3 prior to 16.11.5, 17.0 prior to 17.0.3, and 17.1 prior to 17.1.1. This condition allows for an attacker to abuse the OAuth authentication flow via a crafted payload. This is a medium severity issue (`CVSS:3.1/AV:N/AC:H/PR:N/UI:R/S:U/C:H/I:H/A:N`, 6.8). It is now mitigated in the latest release and is assigned [CVE-2024-2177](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2177).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Project maintainers can bypass group's merge request approval policy

An issue was discovered in GitLab CE/EE affecting all versions starting from 16.10 prior to 16.11.5, starting from 17.0 prior to 17.0.3, and starting from 17.1 prior to 17.1.1, which allows a project maintainer can delete the merge request approval policy via graphQL. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:C/C:N/I:H/A:N`, 6.8).
It is now mitigated in the latest release and is assigned [CVE-2024-5430](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-5430).

Thanks [js_noob](https://hackerone.com/js_noob) for reporting this vulnerability through our HackerOne bug bounty program.


### ReDoS via custom built markdown page

A Denial of Service (DoS) condition has been discovered in GitLab CE/EE affecting all versions from 7.10 prior before 16.11.5, version 17.0 before 17.0.3, and 17.1 before 17.1.1. It is possible for an attacker to cause a denial of service using a crafted markdown page. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-4025](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-4025).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.

### Private job artifacts can be accessed by any user

An issue was discovered in GitLab CE/EE affecting all versions starting from 16.7 prior to 16.11.5, starting from 17.0 prior to 17.0.3, and starting from 17.1 prior to 17.1.1, which allows private job artifacts can be accessed by any user. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N`, 6.5). It is now mitigated in the latest release and is assigned [CVE-2024-3959](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-3959).

Thanks [js_noob](https://hackerone.com/js_noob) for reporting this vulnerability through our HackerOne bug bounty program.


### Security fixes for banzai pipeline

Multiple Denial of Service (DoS) issues has been discovered in GitLab CE/EE affecting all versions starting from 1.0 prior to 16.11.5, starting from 17.0 prior to 17.0.3, and starting from 17.1 prior to 17.1.1 which allowed an attacker to cause resource exhaustion via banzai pipeline. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5). It is now mitigated in the latest release and is assigned [CVE-2024-4557](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-4557).

Thanks [joaxcar](https://hackerone.com) and [setiawan_](https://hackerone.com/setiawan_) for reporting these vulnerability through our HackerOne bug bounty program


### ReDoS in dependency linker

An issue was discovered in GitLab CE/EE affecting all versions starting from 9.2 prior to 16.11.5, starting from 17.0 prior to 17.0.3, and starting from 17.1 prior to 17.1.1, with the processing logic for generating link in dependency files can lead to a regular  expression DoS attack on the server. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5). It is now mitigated in the latest release and is assigned [CVE-2024-1493](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1493).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Denial of service using a crafted OpenAPI file

An issue was discovered in GitLab CE/EE affecting all versions starting from 12.0 prior to 16.11.5, starting from 17.0 prior to 17.0.3, and starting from 17.1 prior to 17.1.1, which allows for an attacker to cause a denial of service using a crafted OpenAPI file. This is a medium severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:N/A:H`, 5.3). It is now mitigated in the latest release and is assigned [CVE-2024-1816](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1816).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Merge request title disclosure

An issue was discovered in GitLab CE/EE affecting all versions starting from 16.9 prior to 16.11.5, starting from 17.0 prior to 17.0.3, and starting from 17.1 prior to 17.1.1, which allows merge request title to be visible publicly despite being set as project members only. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N`, 5.3).
It is now mitigated in the latest release and is assigned [CVE-2024-2191](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2191).

Thanks [ashish_r_padelkar](https://hackerone.com/ashish_r_padelkar) for reporting this vulnerability through our HackerOne bug bounty program.


### Access issues and epics without having an SSO session

An issue was discovered in GitLab EE affecting all versions starting from 16.0 prior to 16.11.5, starting from 17.0 prior to 17.0.3, and starting from 17.1 prior to 17.1.1, which allows an attacker to access issues and epics without having an SSO session using Duo Chat. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N`, 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-3115](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-3115).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Non project member can promote key results to objectives

An issue was discovered in GitLab CE/EE affecting all versions starting from 16.1 prior to 16.11.5, starting from 17.0 prior to 17.0.3, and starting from 17.1 prior to 17.1.1, which allows non-project member to promote key results to objectives. This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:L/A:N`, 3.1). It is now mitigated in the latest release and is assigned [CVE-2024-4011](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-4011).

Thanks [ashish_r_padelkar](https://hackerone.com/ashish_r_padelkar) for reporting this vulnerability through our HackerOne bug bounty program.


## Bug fixes

### 17.1.1

* [Prevent cng e2e test from running in security fork](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/156799)
* [Only enumerate commits in pre-receive check if push came from Web](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157027)
* [Revert "Allow `admin_runner` ability to change shared runners setting"](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/156968)

### 17.0.3

* [Fix missing filename when downloading generic package in release page](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155704)
* [Update an expired test certificate](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/156392)
* [Prevent starting multiple Capybara proxy servers](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/156424)
* [Backport 3 commits for Merge Train pipelines support in 17-0-stable-ee](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/156482)
* [Fix error when calling GQL ciConfig endpoint with include:component](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/156635)
* [Only allow documented token types for GraphQL authentication](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155926)
* [Add a banner informing about token expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155242)
* [Only enumerate commits in pre-receive check if push came from Web](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157026)
* [Backport QA test fixes for stable branches](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7705)
* [Merge branch 'sh-patch-inspec-gem' into 'master'](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7724)

### 16.11.5

* [Prevent starting multiple Capybara proxy servers](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/156425)
* [Update an expired test certificate](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/156393)
* [Enable invert_emails_disabled_to_emails_enabled by default](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/156746)
* [Only allow documented token types for GraphQL authentication](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155925)
* [Add a banner informing about token expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155265)
* [Backport QA test fixes for stable branches](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7704)

### 16.10.8

* [Add a banner informing about token expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155266)

### 16.9.9

* [Add a banner informing about token expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155267)

### 16.8.8

* [Add a banner informing about token expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155268)

### 16.7.8

* [Add a banner informing about token expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155269)

### 16.6.8

* [Add a banner informing about token expiration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155270)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
