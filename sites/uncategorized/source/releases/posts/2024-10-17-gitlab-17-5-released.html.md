---
release_number: "17.5" # version number - required
title: "GitLab 17.5 released with Duo Quick Chat AI code assistance." # short title (no longer than 62 characters) - required
author: John Crowley # author name and surname - required
author_gitlab: johncrowley # author's gitlab.com username - required
image_title: '/images/17_5/17_5-cover-image.png' # cover image - required
description: "GitLab 17.5 released with code assistance in IDEs from GitLab Duo Quick Chat, self-hosted models for GitLab Duo Code Suggestions, export code suggestion usage events, MR conversations with GitLab Duo Chat and much more!" # short description - required
twitter_image: '/images/17_5/17_5-cover-image.png' # required - copy URL from image title section above
categories: releases # required
layout: release # required
featured: yes
rebrand_cover_img: true

# APPEARANCE
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
# release_number_image: "/images/X_Y/X_Y-release-number-image.svg" # uncomment if you want a svg image to replace the release number that normally overlays the background image

---

Today, we are excited to announce the release of GitLab 17.5 with [code assistance in IDEs from GitLab Duo Quick Chat](#introducing-duo-quick-chat), [self-hosted models for GitLab Duo Code Suggestions](#use-self-hosted-model-for-gitlab-duo-code-suggestions), [export code suggestion usage events](#export-code-suggestion-usage-events), [MR conversations with GitLab Duo Chat](#have-a-conversation-with-gitlab-duo-chat-about-your-merge-request) and much more!

These are just a few highlights from the 125+ improvements in this release. Read on to check out all of the great updates below.

To the wider GitLab community, thank you for the 200 contributions you provided to GitLab 17.5!
At GitLab, [everyone can contribute](https://about.gitlab.com/community/contribute/) and we couldn't have done it without you!

To preview what's coming in next month’s release, check out our [Upcoming Releases page](/direction/kickoff/), which includes our 17.6 release kickoff video.
