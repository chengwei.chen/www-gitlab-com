# GitLab All-Company Meetings has been migrated

This handbook content has been migrated to the new handbook site and as such this directory
has been locked from further changes.

Viewable content: [https://handbook.gitlab.com/handbook/company/gitlab-all-company-meetings](https://handbook.gitlab.com/handbook/company/gitlab-all-company-meetings)
Repo Location: [https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/company/gitlab-all-company-meetings](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/company/gitlab-all-company-meetings)

If you need help or assistance with this, please reach out on Slack in
[`#handbook`](https://gitlab.slack.com/archives/C81PT2ALD), or to the
[current handbook DRI](https://handbook.gitlab.com/handbook/about/maintenance/).

